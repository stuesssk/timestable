#include <stdio.h>
#include <math.h>
#include <stdlib.h>


void dash(int padding, int max, int min);

int main(int argc, char* argv[])
{
    // Default values for min and max.
    int min = 0;
    int max = 10;
    int user_min = 0;
    


    // Handling for 1,2,3 CLI arguments.
    // strtol handles any non integer with nonintergers and negative
    // values failing the test.
    // Setting min and max product values based on CLI arugments given.
    switch (argc){
        // argc = 1 default 1-10 products
        // argc = 2 min is 0 and max = argv[1]
        // argc = 3 min = argv[1] and max = argv[2]
        case 1:
            // just the file executing if argc == 1
            break;
        case 2:
            // One other argument is read as maximum
            if (strtol(argv[1], NULL, 10) > 0){
                min = 0;
                max = strtol(argv[1], NULL, 10);
                break;

            }else{
                // Quit if argument is not a postive integer
                // i.e negative and non-integer value
                printf("Usage: %s <values> not a positive integer\n", argv[0]);
                return 1;
            }      
        case 3:
            // argv[1] is min and argv[2] is max
            if (strtol(argv[1], NULL, 0) > 0 && strtol(argv[2], NULL, 10) > 1){
                // Need to compare the raw value for error handling
                user_min = strtol(argv[1], NULL, 10);
                // Need to start one less than min input on the for loops.
                // equivilent of postion zero in the index of the loop
                min = user_min-1;
                max = strtol(argv[2], NULL, 10);
                break;

            }else{
                // Quit if arguments are not postive integers
                // i.e negative and non-integer values
                printf("Usage: %s <values> not a positive integers\n", argv[0]);
                return 1;
            }
        // default is you entered to many argumants
        default:
            printf("Usage: %s <values> more then 2 arguments\n", argv[0]);
            return 1;
            }

        // Quits program if min is greater than max in CLI argument.
        if (user_min >= max){
                printf("Usage: %s <values> min >= max\n", argv[0]);
                return 1;
        }
        
        // Quits program if maximum value is greater than 100
        if (max >100){
            printf("Usage: %s <values> max > 100\n", argv[0]);
            return 1;
        }
    
    // Log10 is great at calulating one less than the total number of 
    // digits in base ten.  floor gives the whol number and +2 acounts for the
    // one less and the extra space of padding needed
    // log10
    int pad = floor(log10(max*max))+2;

    

    for (int row = min; row <= max; ++row){
        // Need to handle new lines row header and special character before 
        // anything else is printed in the row       
        if (row >= user_min && row !=0){
            printf("\n%*d┃",2,row);
        }
        for (int column = min; column <= max; ++column){

            if (row == min){

                if (column == min){
                    printf (" ✖ ");
                }else if (column >= min){
                    // Printing the top row of factors
                    printf("%*d",pad,column);
                }

                
            }else if (column!=min){
                // Printing the factors.
                printf("%*d",pad,row*column);
            }
        }
        // the row of pretty Unicode characters was easiest to handle as a
        // seperate function
        if (row == min){
            dash(pad,max,min);
        }
    }
    // Print newline after each row of factors.
    printf("\n");
    
            
}





// Pretty line below row products function call.
void dash(int padding, int max, int min)
{
    // Need a new line and some special char of the bat.
    printf("\n  ┏");

    // Max = number of number to print and padding is width of field 
    // for each number 
    for (int i = min; i < max; ++i){
        for (int j = 0; j < padding;++j){
            // Print the pretty Unicode character.                
            printf("━");
        }
    }
}
