CPPFLAGS+=-Wall -Wextra -Wpedantic -Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline\

CFLAGS+=-std=c11

LDLIBS+=-lm

TARGET=timestable

.PHONY: all

all: $(TARGET)



clean:
	$(RM) $(TARGET)
